-- MySQL dump 10.13  Distrib 5.1.41, for Win32 (ia32)
--
-- Host: localhost    Database: crm
-- ------------------------------------------------------
-- Server version	5.1.41-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers_tb`
--

DROP TABLE IF EXISTS `customers_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `type_manage_id` int(10) NOT NULL DEFAULT '1',
  `termination_date` datetime DEFAULT NULL,
  `termination_reason_manage_id` int(10) NOT NULL DEFAULT '1',
  `termination_reason_etc` varchar(255) DEFAULT NULL,
  `using_product_manage_id` int(10) NOT NULL DEFAULT '0',
  `using_product_etc` varchar(255) DEFAULT NULL,
  `customer_info_id` int(10) NOT NULL DEFAULT '1',
  `sales_personality_id` int(10) NOT NULL DEFAULT '1',
  `spare_server_manage_id` int(10) NOT NULL DEFAULT '0',
  `corp_name` varchar(40) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `reseller` varchar(40) DEFAULT NULL,
  `tax_bill_publisher` varchar(255) DEFAULT NULL,
  `home_page_url` varchar(255) DEFAULT NULL,
  `reg_date` datetime DEFAULT NULL,
  `corp_reg_num` varchar(255) NOT NULL,
  `manage_num` varchar(255) NOT NULL,
  `admin_id` varchar(40) NOT NULL,
  `customer_access_url` varchar(255) NOT NULL,
  `business_type_manage_id` int(10) NOT NULL DEFAULT '1',
  `pc_agent_cnt` int(10) NOT NULL DEFAULT '0',
  `mobile_agent_cnt` int(10) NOT NULL DEFAULT '0',
  `total_agent_cnt` int(10) NOT NULL DEFAULT '0',
  `reg_ship_addr` varchar(255) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `contract_doc` varchar(255) DEFAULT NULL,
  `pay_content` varchar(255) DEFAULT NULL,
  `remark` mediumtext,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer''s Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `directors_tb`
--

DROP TABLE IF EXISTS `directors_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directors_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `director_type_id` int(10) NOT NULL DEFAULT '1',
  `name` varchar(40) DEFAULT NULL,
  `position` varchar(40) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `tel1` varchar(40) DEFAULT NULL,
  `tel2` varchar(40) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `customer_fk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='director''s Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hs_pay_tb`
--

DROP TABLE IF EXISTS `hs_pay_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hs_pay_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `agent_id` varchar(40) NOT NULL,
  `pc_cnt` int(10) NOT NULL DEFAULT '0',
  `mobile_cnt` int(10) NOT NULL DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `product_cnt` int(10) NOT NULL DEFAULT '0',
  `term` int(10) NOT NULL,
  `sum_price` int(10) NOT NULL,
  `unit_price` int(10) NOT NULL,
  `staff_manage_id` int(10) NOT NULL DEFAULT '1',
  `dealer_name` varchar(40) DEFAULT NULL,
  `dealer_tel` varchar(255) DEFAULT NULL,
  `dealer_mail` varchar(255) DEFAULT NULL,
  `deal_type_manage_id` int(10) NOT NULL DEFAULT '1',
  `pay_doc` mediumtext,
  `customer_fk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Payment History ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `info_auth_tb`
--

DROP TABLE IF EXISTS `info_auth_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info_auth_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `menage_id` int(10) NOT NULL DEFAULT '0',
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='User''s Auth Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `info_business_type_tb`
--

DROP TABLE IF EXISTS `info_business_type_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info_business_type_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `manage_id` int(10) NOT NULL DEFAULT '1',
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Business Type Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `info_customer_type_tb`
--

DROP TABLE IF EXISTS `info_customer_type_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info_customer_type_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `manage_id` int(10) NOT NULL DEFAULT '1',
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Type Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `info_deal_type_tb`
--

DROP TABLE IF EXISTS `info_deal_type_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info_deal_type_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `manage_id` int(10) NOT NULL DEFAULT '1',
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Deal Type Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `info_spare_server_tb`
--

DROP TABLE IF EXISTS `info_spare_server_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info_spare_server_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `menage_id` int(10) NOT NULL DEFAULT '0',
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Spare Server''s Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `info_staff_tb`
--

DROP TABLE IF EXISTS `info_staff_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info_staff_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `manage_id` int(10) NOT NULL DEFAULT '1',
  `position` int(40) DEFAULT NULL,
  `gender` int(10) DEFAULT '1',
  `name` int(40) NOT NULL,
  `name_num` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Staff''s Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `info_termination_reason_tb`
--

DROP TABLE IF EXISTS `info_termination_reason_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info_termination_reason_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `manage_id` int(10) NOT NULL DEFAULT '1',
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Termination Reason Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `info_using_product_tb`
--

DROP TABLE IF EXISTS `info_using_product_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info_using_product_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `manage_id` int(10) NOT NULL DEFAULT '0',
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Using Product Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_tb`
--

DROP TABLE IF EXISTS `users_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_tb` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `id` varchar(40) NOT NULL,
  `pw` varchar(255) NOT NULL,
  `type_id` int(10) NOT NULL DEFAULT '1',
  `auth_manage_id` int(10) NOT NULL DEFAULT '0',
  `flag_status` tinyint(1) NOT NULL DEFAULT '1',
  `staff_manage_id` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='User''s Information';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-14 15:37:04
